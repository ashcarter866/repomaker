# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Leviatansan21 <jorgeivanponcehernandez@yahoo.co.jp>, 2020.
# Hans-Christoph Steiner <hans@guardianproject.info>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-09-01 20:54+0000\n"
"PO-Revision-Date: 2020-04-29 12:49+0000\n"
"Last-Translator: Hans-Christoph Steiner <hans@guardianproject.info>\n"
"Language-Team: Spanish (Mexico) <https://hosted.weblate.org/projects/f-droid/repomaker/es_MX/>\n"
"Language: es_MX\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.0.2\n"

#, python-format
msgid "This file is not an update for %s"
msgstr "Este archivo no es una actualización para %s"

msgid "This APK already exists in the current repo."
msgstr "Este APK ya existe en el repositorio actual."

msgid "Invalid APK signature"
msgstr "La Firma APK es inválida"

msgid "Invalid APK."
msgstr "APK invalida"

msgid "Unsupported File Type"
msgstr "Tipo de archivo no soportado"

msgid "This file is of a different type than the other versions of this app."
msgstr "Este archivo es de un tipo diferente que las otras versiones de esta aplicación."

#, fuzzy
msgid "APK"
msgstr "APK"

msgid "Book"
msgstr "Libro"

msgid "Document"
msgstr "Documento"

msgid "Image"
msgstr "imagen"

#, fuzzy
msgid "Audio"
msgstr "Audio"

#, fuzzy
msgid "Video"
msgstr "Video"

msgid "Other"
msgstr "Otro"

msgid "This app does already exist in your repository."
msgstr "Esta aplicación ya existe en su repositorio."

msgid "This app does not have any working versions available."
msgstr "Esta aplicación no tiene ninguna versión de trabajo disponible."

msgid "Phone"
msgstr "Celular"

msgid "7'' Tablet"
msgstr "7'' Tablet"

#, fuzzy
msgid "10'' Tablet"
msgstr "10'' Tablet"

#, fuzzy
msgid "TV"
msgstr "TV"

msgid "Wearable"
msgstr "Se puede usar"

msgid "US Standard"
msgstr "Estándar de EE. UU."

msgid "Amazon S3 Storage"
msgstr "Almacenamiento de Amazon S3"

msgid "Enter a valid user name consisting of letters, numbers, underscores or hyphens."
msgstr "Introduzca un nombre de usuario válido que consista en letras, números, guiones bajos o guiones."

msgid "Enter a valid hostname."
msgstr "Introduzca un nombre de host válido."

msgid "Enter a valid path."
msgstr "Introduzca una ruta válida."

msgid "SSH Storage"
msgstr "Almacenamiento SSH"

msgid "Git Storage"
msgstr "Almacenamiento Git"

msgid "Default Storage"
msgstr "Almacenamiento predeterminado"

msgid "American English"
msgstr "Inglés americano"

msgid "Change E-mail"
msgstr "Cambiar correo electrónico"

msgid "Sign Out"
msgstr "Cerrar sesión"

msgid "Sign In"
msgstr "Inicia sesión"

msgid "Sign Up"
msgstr "Regístrate"

msgid "Distribute apps."
msgstr "Distribuir aplicaciones."

msgid "Spread the love."
msgstr "Difunde el amor."

msgid "Repomaker is a free web app built for creating collections of apps, music, books, videos and pictures to share with your peers. It uses F-Droid as the mobile distribution platform. Available on Android only."
msgstr "Repomaker es una aplicación web gratuita creada para crear colecciones de aplicaciones, música, libros, vídeos e imágenes para compartir con sus compañeros. Utiliza F-Droid como la plataforma de distribución móvil. Disponible solo en Android."

msgid "Login"
msgstr "Iniciar sesión"

msgid "Forgot Password"
msgstr "Olvidé la contraseña"

msgid "Or login with"
msgstr "O inicie sesión con"

msgid "Logout"
msgstr "Cerrar sesión"

msgid "See you later"
msgstr "Nos vemos luego"

msgid "Your security is important to us. Please confirm that you want to logout."
msgstr "Su seguridad es importante para nosotros. Confirme que desea cerrar la sesión."

msgid "logout"
msgstr "Cerrar sesión"

msgid "submit"
msgstr "Enviar"

msgid "We have sent you an email with a link to reset your password."
msgstr "Le hemos enviado un correo electrónico con un enlace para restablecer su contraseña."

msgid "Signup"
msgstr "Regístrate"

msgid "Delete App Version"
msgstr "Eliminar la versión de la aplicación"

#, python-format
msgid "Are you sure you want to delete this version <strong>%(version)s</strong> (%(code)s) from your app <strong>%(app)s</strong>?"
msgstr "¿Está seguro de que desea eliminar esta versión <strong> %(version)s</strong> (%(code)s) de su aplicación <strong> %(app)s</strong>?"

msgid "Confirm"
msgstr "Confirmar"

msgid "Delete App"
msgstr "Borrar la aplicación"

#, python-format
msgid "Are you sure you want to delete your app <strong>%(object)s</strong>?"
msgstr "¿Estás seguro de que quieres borrar tu aplicación <strong>%(object)s</strong>?"

#, python-format
msgid "Edit %(app)s"
msgstr "Editar %(app)s"

msgid "Remove App"
msgstr "Eliminar aplicación"

msgid "Done"
msgstr "Listo"

msgid "By"
msgstr "Por"

msgid "Summary Override (40 characters)"
msgstr "Anulación del resumen (40 caracteres)"

msgid "Short Summary (40 characters)"
msgstr "Resumen corto (40 caracteres)"

msgid "Category"
msgstr "Categoría"

msgid "Choose Category"
msgstr "Elegir la categoría"

msgid "Description Override"
msgstr "Descripción Reemplazar"

msgid "Description"
msgstr "Descripción"

msgid "Screenshots"
msgstr "Capturas de pantalla"

msgid "Add files"
msgstr "Añadir archivos"

msgid "Drag and drop screenshots"
msgstr "Arrastrar y soltar capturas de pantalla"

msgid "or"
msgstr "O"

msgid "browse to upload"
msgstr "Examinar y Subir"

msgid "Feature Graphic"
msgstr "Caracteristicas de Grafico"

msgid "Drag and drop a feature graphic"
msgstr "Arrastre y suelte una Caracteristica de gráfico"

msgid "Drag and drop .apk files"
msgstr "Arrastre y suelte archivos .apk"

msgid "Back"
msgstr "Atrás"

msgid "Editing Disabled"
msgstr "Edición deshabilitada"

msgid "This app gets updated automatically from the remote repo. If you want to edit it yourself, you need to disable automatic updates first. Please note that without automatic updates, you will need to add new versions manually."
msgstr "Esta aplicación se actualiza automáticamente desde el repositorio remoto. Si desea editarlo usted mismo, primero debe deshabilitar las actualizaciones automáticas. Tenga en cuenta que sin actualizaciones automáticas, tendrá que agregar nuevas versiones manualmente."

msgid "Disable Automatic Updates"
msgstr ""

msgid "Delete Feature Graphic"
msgstr ""

#, python-format
msgid "Are you sure you want to delete the feature graphic from your app <strong>%(app)s</strong>?"
msgstr ""

#, python-format
msgid "Languages (%(count)s)"
msgstr ""

msgid "Edit"
msgstr ""

msgid "Add Translation"
msgstr ""

#, python-format
msgid "By %(author)s"
msgstr ""

#, python-format
msgid "Updated %(time)s ago"
msgstr ""

#, python-format
msgid "This app gets automatically updated from the remote repo \"%(repo_name)s\"."
msgstr ""

msgid "version history"
msgstr ""

#, python-format
msgid "Version %(version)s (%(code)s)"
msgstr ""

#, python-format
msgid "Released %(date)s"
msgstr ""

msgid "APK is still downloading..."
msgstr ""

msgid "This app has no APK files."
msgstr ""

msgid "If you added this app from a remote repo, try deleting it and adding it again."
msgstr ""

msgid "Previous"
msgstr ""

msgid "Next"
msgstr ""

msgid "Add"
msgstr ""

msgid "There are currently no screenshots shown because this gives the repo owner the ability to track you."
msgstr ""

msgid "show screenshots"
msgstr ""

msgid "Delete Screenshot"
msgstr ""

#, python-format
msgid "Are you sure you want to delete this screenshot from your app <strong>%(app)s</strong>?"
msgstr ""

msgid "Language Code"
msgstr ""

#, python-format
msgid "Hello, %(user)s"
msgstr ""

msgid "Please Wait!"
msgstr ""

msgid "Background Operation in Progress"
msgstr ""

msgid "Repomaker might be updating or publishing your repo. Or it might be updating a remote repo. Depending on the type and size of the background operation, this might take awhile."
msgstr ""

msgid "Try Again"
msgstr ""

msgid "The button won't work for you. Please press F5 to try again."
msgstr ""

msgid "Error"
msgstr ""

msgid "My Repos"
msgstr ""

#, python-format
msgid "Published %(time_ago)s"
msgstr ""

#, python-format
msgid "Created %(time_ago)s"
msgstr ""

msgid "Make a collection of apps to share"
msgstr ""

msgid "Create Repo"
msgstr ""

msgid "New Repo"
msgstr ""

msgid "Create"
msgstr ""

msgid "Delete Repo"
msgstr ""

#, python-format
msgid "Are you sure you want to delete the repo <strong>%(repo)s</strong>?"
msgstr ""

msgid "This will also delete all apps and other files from your repo. You will not be able to get them back, users of your repo will not be able to use it anymore and not receive updates for the apps they received from you. There is no way to recover this repo!"
msgstr ""

msgid "Please only proceed if you know what you are doing!"
msgstr ""

msgid "Yes, I want to delete the repo"
msgstr ""

msgid "Edit Repo"
msgstr ""

msgid "Save Changes"
msgstr ""

msgid "Content"
msgstr ""

msgid "Share"
msgstr ""

msgid "Info"
msgstr ""

msgid "Search for apps in your repository"
msgstr ""

msgid "add from gallery"
msgstr ""

#, python-format
msgid "Alternatively, drag and drop or %(label_start)schoose files%(label_end)s"
msgstr ""

msgid "Accepted files include Android apps, documents, eBooks, audio and video."
msgstr ""

msgid "Your search did not return any results."
msgstr ""

msgid "Name"
msgstr ""

msgid "Storage"
msgstr ""

msgid "Add Storage"
msgstr ""

msgid "Fingerprint"
msgstr ""

msgid "Remove Repo"
msgstr ""

msgid "Add storage to publish"
msgstr ""

msgid "Your repo will be automatically published when you add storage. Repomaker does not publically promote your stuff. Only people who receive the direct link to your repo will be able to see it."
msgstr ""

msgid "add storage"
msgstr ""

#, python-format
msgid "You have  <span>%(app_count)s app</span> in your repo. Share it!"
msgid_plural "You have  <span>%(app_count)s apps</span> in your repo. Share it!"
msgstr[0] ""
msgstr[1] ""

msgid "view repo"
msgstr ""

msgid "share public link"
msgstr ""

msgid "copy link"
msgstr ""

msgid "add to an android phone"
msgstr ""

msgid "To install these apps on your Android phone, add them to F-Droid."
msgstr ""

msgid "F-Droid ensures access to secure app updates."
msgstr ""

msgid "Scan QR"
msgstr ""

#, python-format
msgid "Check out this F-Droid repo: %(name)s"
msgstr ""

msgid "Browse Gallery"
msgstr ""

msgid "Search"
msgstr ""

#, python-format
msgid "Category (%(count)s)"
msgstr ""

msgid "All"
msgstr ""

#, python-format
msgid "Source (%(count)s)"
msgstr ""

msgid "Add a repo"
msgstr ""

msgid "Clear filters"
msgstr ""

#, python-format
msgid "No apps found that match '%(search_term)s'. Please try a different search or remove other filter criteria."
msgstr ""

msgid "There are no apps in this repo."
msgstr ""

msgid "There are no apps available in the repos."
msgstr ""

msgid "If you just added a repo, wait for the apps to download and try again later."
msgstr ""

msgid "This repo has no apps in the selected category. Try clearing it or select a different repo."
msgstr ""

msgid "There are no apps available in the selected category. Try clearing it."
msgstr ""

msgid "Delete Storage"
msgstr ""

#, python-format
msgid "Are you sure you want to remove the storage at <strong>%(object)s</strong> from your repo?"
msgstr ""

msgid "This will not remove the content from that storage but will stop to publish future versions of your repo to it."
msgstr ""

msgid "Enable Storage"
msgstr ""

msgid "Disable Storage"
msgstr ""

msgid "Repo Address"
msgstr ""

msgid "Edit Storage"
msgstr ""

msgid "Please make sure to add this public SSH key to your Git account for the upload to work. There are instructions for <a href=\"https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html#how-to-create-your-ssh-keys\">Gitlab</a> and <a href=\"https://developer.github.com/v3/guides/managing-deploy-keys/#deploy-keys\">GitHub</a>."
msgstr ""

msgid "Also, please make sure that the master branch of your repository is <b>not protected</b> from force pushing."
msgstr ""

msgid "Git Repository SSH Address"
msgstr ""

msgid "Region"
msgstr ""

msgid "Bucket"
msgstr ""

msgid "Access Key ID"
msgstr ""

msgid "Please make sure to add this public SSH key to your SSH server's <tt> <a href=\"https://en.wikibooks.org/wiki/OpenSSH/Client_Configuration_Files#.7E.2F.ssh.2Fauthorized_keys\">~/.ssh/authorized_keys</a> </tt> file,\tif you have not done so already."
msgstr ""

msgid "Security Warning: This gives repomaker write access to that storage. Please limit this user's access as much as possible."
msgstr ""

msgid "Username"
msgstr ""

msgid "Host"
msgstr ""

msgid "Path"
msgstr ""

msgid "Your local default SSH key will be used to connect to the storage."
msgstr ""

msgid "Please make sure to authorize this public SSH key for your remote storage, if you have not done so already."
msgstr ""

msgid "Activate this storage only once you have added the SSH key."
msgstr ""

msgid "If this storage is experiencing problems, please check that the SSH key works properly."
msgstr ""

#, python-format
msgid "Update %(storage_name)s"
msgstr ""

#, python-format
msgid "Setup %(storage_name)s"
msgstr ""

msgid "cancel"
msgstr ""

msgid "save"
msgstr ""

msgid "Here's how to copy the SSH Address from GitLab:"
msgstr ""

msgid "Setup a place to store your repo"
msgstr ""

msgid "Git"
msgstr ""

msgid "Easy, free, requires an account"
msgstr ""

msgid "Git is a popular tool to version control things. You can securely publish repos to GitLab, GitHub or any other Git service. Git services use SSH to securely copy files from your computer to remote computers."
msgstr ""

msgid "Setup"
msgstr ""

msgid "GitLab"
msgstr ""

msgid "GitHub"
msgstr ""

msgid "SSH"
msgstr ""

msgid "Requires a personal web server or host that supports SSH"
msgstr ""

msgid "SSH is a technology that can be used to copy files securely to remote computers. When using Git as storage, the files will also be copied via SSH. But the pure SSH option directly copies the files without using any other technology."
msgstr ""

msgid "Amazon S3"
msgstr ""

msgid "Requires an Amazon account, Pricing varies"
msgstr ""

msgid "Amazon S3 is a web service offered by Amazon Web Services. Amazon S3 provides storages through web service interfaces. Pricing varies. Visit the Amazon S3 website for details."
msgstr ""

#, python-format
msgid "Version %(version)s (%(date)s)"
msgstr ""

msgid "Added"
msgstr ""

msgid "There was an error creating the repository. Please try again!"
msgstr ""

msgid "Enter email"
msgstr ""

msgid "Enter your email address and we'll send a link to reset it. If you did not sign up with an email, we cannot help you securely reset your password."
msgstr ""

msgid "This is not a valid language code."
msgstr ""

msgid "This language already exists. Please choose another one!"
msgstr ""

msgid "Enter SSH URL"
msgstr ""

msgid "Raw Git URL"
msgstr ""

msgid "This is the location where the uploaded files can be accessed from in raw form. Leave empty for GitHub or GitLab.com repositories."
msgstr ""

msgid "This URL must start with 'git@'."
msgstr ""

msgid "This URL must end with '.git'."
msgstr ""

msgid "This URL is invalid. Please copy the exact SSH URL of your git repository."
msgstr ""

msgid "Please add a valid URLfor the raw content of this git repository."
msgstr ""

msgid "Repository URL"
msgstr ""

msgid "Please use a URL with a fingerprint at the end, so we can validate the authenticity of the repository."
msgstr ""

msgid "Please don't add one of your own repositories here."
msgstr ""

#, python-format
msgid "Could not validate repository: %s"
msgstr ""

msgid "Describe the repo"
msgstr ""

msgid "Bucket Name"
msgstr ""

msgid "Secret Access Key"
msgstr ""

msgid "Other regions are currently not supported."
msgstr ""

msgid "Language"
msgstr ""

msgid "Select Screenshot for upload"
msgstr ""

msgid "Use local default SSH Key"
msgstr ""

msgid "User Name"
msgstr ""

msgid "URL"
msgstr ""

msgid "This is the location where the uploaded files can be accessed from."
msgstr ""

msgid "Primary Storage"
msgstr ""

msgid "Make this storage the primary storage of the repository."
msgstr "Haga de este almacenamiento el almacenamiento principal del repositorio."
